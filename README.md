Installation

1. Composer Update
2. Setup Env
	- Database
	- SMTP
	- Queue (QUEUE_CONNECTION=database)
3. run 'php artisan migrate'
4. run 'php artisan db:seed'
5. run 'php artisan serve'
6. run 'php artisan queue:listen'

Login Admin
admin@huntstreet.com | 12345678