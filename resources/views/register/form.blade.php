<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    <title>FORM UNDANGAN EVENT HUNTBAZAAR</title>
    <!-- Custom CSS -->
    <link href="{{ asset('dist/css/style.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">    
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box">
                <div>
                    <div class="logo">
                        <h4 class="font-medium m-b-20">
                            PELAKSANAAN EVENT PADA TANGGAL 24 DESEMBER 2020
                            <br>
                            <br>
                            BATAS WAKTU PENDAFTARAN TANGGAL 20 DESEMBER 2020
                            <br>
                            <br>
                            <div class="timerDown"></div>
                            <br>
                            PENGISIAN FORM UNDANGAN EVENT HUNTBAZAAR
                            <br>
                        </h4>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" action="{{ route('registration.save', [$invite->token]) }}" method="POST">
                                @csrf
                                @include('partials.alert')
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="">Email</label>
                                        <input class="form-control form-control-lg" type="email" name="email" required="" value="{{ $invite->email }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="">Nama Lengkap</label>
                                        <input class="form-control form-control-lg" type="text" name="nama" required value="{{ @old('nama') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="">Tanggal Lahir</label>
                                        <input class="form-control form-control-lg" type="date" name="tanggal" required value="{{ @old('tanggal') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="">Jenis Kelamin</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="pria" name="jk" value="L" class="custom-control-input">
                                            <label class="custom-control-label" for="pria">Pria</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="wanita" name="jk" value="P" class="custom-control-input">
                                            <label class="custom-control-label" for="wanita">Wanita</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="">Designer Favorit</label>
                                        {!! Form::select('designer[]', $designers, '', ['id' => 'designer', 'class' => 'form-control select2', 'multiple' => true, 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <div class="col-xs-12 p-b-20">
                                        <button class="btn btn-block btn-lg btn-info" type="submit">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>    
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();

    $(function () {
        $('#designer').select2({
            placeholder: " Silahkan Pilih Designer Favorit Anda"
        });

        var dtTime = '{{ $countdown }}';
        //alert(dtTime);
        var timer2 =  dtTime;
        var interval = setInterval(function () {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var days = parseInt(timer[0], 10);
            var hours = parseInt(timer[1], 10);
            var minutes = parseInt(timer[2], 10);
            var seconds = parseInt(timer[3], 10);
            --seconds;  

            if (minutes < 0)
                clearInterval(interval);
                
            // Menit
            minutes = (seconds < 0) ? --minutes : minutes;
            // minutes = (minutes < 0) ? 59 : minutes;
            if (minutes < 0) {
                minutes = 59;
                --hours;
            }else{
                hours = hours;
                minutes = minutes;
            }

            if (hours < 0) {
                hours = 23;
                --days;
            }else{
                hours = hours;
                days = days;
            }

            minutes = (minutes < 10) ? '0' + minutes : minutes;
            
            // Hari
            days = (hours < 0) ? --days : days;
            days = (days < 10) ? '0' + days : days; 

            // Jam
            hours = (minutes < 0) ? --hours : hours;
            hours = (hours < 10) ? '0' + hours : hours;

            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            // minutes = (minutes < 10) ?  minutes : minutes;
            // console.log(seconds);
            $('.timerDown').html(days + ':' + hours + ':' + minutes + ':' + seconds);
            timer2 = days + ':' + hours + ':' + minutes + ':' + seconds;
        }, 1000);        
    });
    </script>
</body>

</html>