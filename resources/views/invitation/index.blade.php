@extends('layouts.app')

@section('css')
<style>
    .select2-container--classic .select2-selection--single, .select2-container--default .select2-selection--multiple, .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow, .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 36px !important;
        line-height: 36px !important;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">    
@endsection

@section('content')
<div class="container p-3 m-3">
        
        <!-- Page-header start -->
        <div class="page-header card p-2">
            <div class="card-block">
                <h5 class="m-b-10">{{ __('Data Undangan') }}</h5>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="ti-home"></i> {{ __('Dashboard') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Data Undangan') }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Page-header end -->
        @include('partials.alert')
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card p-4">
                        <div class="card-block table-border-style">
                            <a data-toggle="modal" data-target="#modalInviteForm" class="btn btn-primary" rel="noopener noreferrer">New Invite</a>
                            <table class="table w-100" id="data-invite">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Status') }}</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="pull-right">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.modal-invite')

</div>
@endsection
@section('plugin-js')
<script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endsection
@section('script')
@include('partials.datatables')
<script>
    $(document).ready(function(){

        var dataTable = $('#data-invite').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url : "{{ route('invite.data') }}",
            },  
            columns: [
                { data: null, name: '_id' },
                { data: 'email', name: 'email' },
                { data: 'status', name: 'status' },
            ],
        });

        @error('email')
            $('#modalInviteForm').modal('toggle');
        @enderror

    });
</script>
@endsection 
