<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $.extend( true, $.fn.dataTable.defaults, {
        pageLength: 10,
        // scrollX: true,
        scrollCollapse: true,
        dom: '<"row" <"col-md-4" l> <"col-md-3 pt-3"B> <"col-md-4 right"f>>  <"bottom" rtip><"clear">',
        // dom: 'BlfB',//lBfrtip
        buttons: [
            { extend: 'print', exportOptions:
                { columns: [':visible :not(:last-child)'] }
            },
            { extend: 'excel', exportOptions:
                { columns: [':visible :not(:last-child)'] }
            },
        ],
        language: {
            processing: "Loading Data..."
        }, 
        columnDefs: [
            { 
                targets: [ 'actions' ], //first column / numbering column
                orderable: false, //set not orderable
            },
            { 
                targets: [ 'actions' ], //first column / numbering column
                searching: false, //set not orderable
            },
        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        },
        "initComplete": function(settings, json) {
            $(this).find('button.destroy').on('click', function(e){
                var username = $(this).data('user');
                var name = $(this).data('name');
               
                if ( username != undefined ){
                    var html = `you want to delete <b>${username}</b>.`;
                }else if ( name != undefined ){
                    var html = `you want to delete this <b>${name}</b>.`
                }

                e.preventDefault();
                Swal.fire({
                    title: 'Are you sure?',
                    icon: 'question',
                    html: html,
                    showCancelButton: true,
                    confirmButtonColor: '#D33',
                    cancelButtonColor: '#666',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $(this).parent().submit();
                    }
                });

            });
        }
    } );
</script>
