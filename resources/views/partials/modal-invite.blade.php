<div class="modal fade" id="modalInviteForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="{{ route('invite.save') }}" method="POST">
            @csrf
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Kirim Undangan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body mx-3">
                <div class="md-form mb-5">
                  <input type="email" name="email" id="orangeForm-email" class="form-control validate" placeholder="Isi Email">
                  @error('email')
                  <div class="">
                    {{ $message }}
                  </div>
                  @enderror
                </div>
        
              </div>
              <div class="modal-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-deep-orange">Send</button>
              </div>
        </form>
    </div>
  </div>
</div>