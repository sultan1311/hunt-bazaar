<?php

use Illuminate\Support\Facades\Route;
use App\Jobs\SendEmailSuccess;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (!auth()->check()) {
        return redirect('/login');
    }else {
        return redirect('/dashboard');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::get('/invite', 'InvitationController@index')->name('invite.index');
    Route::get('/invite/data', 'InvitationController@data')->name('invite.data');
    Route::post('/invite/store', 'InvitationController@store')->name('invite.save');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/invitations/{token}', 'RegisterController@form')->name('invitations');
    Route::post('/registration/{token}', 'RegisterController@store')->name('registration.save');
});
