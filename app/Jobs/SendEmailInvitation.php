<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\InvitationEmail;
use Mail;
use App\Invitation;
use App\Status;

class SendEmailInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invite = Invitation::where('email', $this->data['email'])->firstOrFail();

        if ($invite) {
            $email = new InvitationEmail($this->data);
            Mail::to($this->data['email'])->send($email);

            $status = Status::find(2);
            $invite->status()->associate($status);
            $invite->save();
        }
    }
}
