<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SendMailSuccess;
use App\Registration;
use App\Invitation;
use Mail;

class SendEmailSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $register = Registration::findOrFail($this->id);
        $email = new SendMailSuccess($register);
        Mail::to($register->email)->send($email);

        $invite = Invitation::where('email', $register->email)->firstOrFail();
        $status = Status::find(3);
        $invite->status()->associate($status);
        $invite->save();
    }
}
