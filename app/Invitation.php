<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;

class Invitation extends Model
{
    protected $table = 'invitations';

    protected $fillable = [
        'email', 'token', 'status_id'
    ];

    public function status(){
        return $this->belongsTo('App\Status', 'status_id', 'id');
     }
}
