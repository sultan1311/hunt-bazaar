<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\formRegisterRequest;
use App\Designer;
use App\Invitation;
use App\Registration;
use App\Jobs\SendEmailSuccess;


class RegisterController extends Controller
{
    public function form($token)
    {
        $invite = Invitation::where('token', '=', $token)->firstOrFail();
        $designers = Designer::all()->pluck('nama', 'id');

        $register = Registration::where('email', $invite->email)->first();

        $view = "register.form";
        $start = date_create(date('Y-m-d H:i:s'));
        $end = date_create('2020-12-20 13:00:00');
        $startTime = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
        $finishTime = \Carbon\Carbon::parse('2020-12-20 13:00:00');

        $countdown = $finishTime->diff($startTime)->format('%D:%H:%I:%S');

        if ($register <> "") {
            $view = "register.success";
        }

        return view($view, compact('designers', 'invite', 'countdown', 'register'));
    }

    public function store($token, formRegisterRequest $request)
    {
        $invite = Invitation::where('token', '=', $token)->firstOrFail();
        
        $data = $request->validated();

        try {
            $registrant = new Registration;
            $registrant->email = $invite->email;
            $registrant->nama = $data['nama'];
            $registrant->tgl_lahir = $data['tanggal'];
            $registrant->jk = $data['jk'];
            $registrant->code =  Str::random(10);
            $registrant->save();
            
            if ($registrant) {
                $registrant->designers()->sync($data['designer']);

                SendEmailSuccess::dispatch($registrant->id)->delay(now()->addMinutes(60));
            }

            $alert = 'Berhasil, anda menjadi peserta event HUNTBAZAAR';
        } catch (\Throwable $th) {
            $alert = 'Ada kesalahan, hubungi Customer Support';
        }

        return redirect()->route('invitations', [$invite->token])->with('alert', $alert)->withInput();   
    }
}
