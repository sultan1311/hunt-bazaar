<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Invitation;
use App\Jobs\SendEmailInvitation;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invitation.index');
    }

    /**
     * Show data
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        $data = Invitation::all();
        return datatables()->of($data)
            ->addColumn('status', function ($data)
            {
                return $data->status->name;
            })->toJson();
        // return datatables(Invitation::all())->toJson();
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->email;

        $rules = [
            //Login information
            'email' => 'required|email:filter|max:155|unique:invitations',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('invite')->withErrors($validator)->withInput();
        }

        try {
            // Store data to db
            $invite = new Invitation();
            $invite->email = $email;
            $invite->token = sha1(time());
            $invite->status_id = 1;
            $invite->save();

            // defer the processing of the sending invitation mail
            SendEmailInvitation::dispatch([
                'email' => $email,
                'link' => route('invitations', [$invite->token])
            ]);

            $alert = 'Berhasil menambahkan undangan';
        } catch (\Throwable $th) {
            $alert = 'Ada kesalahan, hubungi Customer Support';
        }

        return redirect('invite')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
