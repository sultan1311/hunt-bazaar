<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'registrations';

    /**
     * @return BelongsToMany
     */
    public function designers()
    {
        return $this->belongsToMany('App\Designer', 'registration_designer', 'registration_id', 'designer_id');
    }    
}
