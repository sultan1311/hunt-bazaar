<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    public function invite()
    {
        return $this->hasMany('App\Invitation', 'status_id', 'id');
    }
}
