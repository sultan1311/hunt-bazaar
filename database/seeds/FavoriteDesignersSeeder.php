<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FavoriteDesignersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designers = [
            "A Bathing Ape",  
            "A.L.C",  
            "A.P.C",  
            "A.W.A.K.E",  
            "AAPE by A Bathing Ape", 
            "ACG",  
            "Acne Studios",  
            "Adam by Adam Lippes",  
            "Adam Lippes",  
            "Adidas",  
            "Adidas by Rick Owens",  
            "Adidas by Y-3 Yohji Yamamoto",  
            "Adrian Gan",  
            "Adrianna Papell",  
            "Adriano Goldschmied",  
            "Adrienne Landau",  
            "Agnès B.",  
            "Aidan And Ice",  
            "Aidan Mattox",  
            "Aigner",  
            "Air Jordan",  
            "Alaïa",  
            "Alain Mikli",  
            "ALALA",  
            "Alberta Ferretti",  
            "Aldies",  
            "Alejandro Ingelmo",  
            "Alessandra Rich",  
            "Alessandro Dell'Acqua",
        ];

        foreach ($designers as $key => $nama) {
            DB::table('designers')->insert(
                [
                'nama' => $nama,
            ]);
        }
    }
}
